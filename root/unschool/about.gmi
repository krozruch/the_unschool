## About The Unschool

### Writing

It's not good sign if two people get all excited about planning their wedding and the marriage it is to celebrate appears to leave them cold. Likewise, it's the journey rather than the destination here and so, if this is for you, it may not be altogether clear whether writing is the means or the end.

### Ownership and Ego

### Thinking out loud

The unschool is a work in progress. In fact, it might be considered that it does not yet exist. There are various name candidates, including the Automat Svět Unschool of Writing, the Taoist Unschool of Writing and the Apprentice Blaitherer Unschool of Writing. (The first and the last of these are rather Czech, influenced by the psychgeography of the New Wave of Czechoslovakian film and Bohumil Hrabal, who less directly influences the second. But that doesn't matter much here.) Beyond the name, its being described as an "unschool" strikes me for the moment as better than its being described as a "school". "Club", "group", and "movement", all strike me as wrong; perhaps "circle" and "cell" are better while "coop" is aspirational. It is or will likely be a "distro" since you could literally start a new one by configuring a server (however you want), literally copying the content (perhaps at a given point in time), and making a few changes. Other influences, then, in no particular order, are Taoism, punk (the broader movement in its more thoughtful forms more so than much of the music), zines, anarchism, samizdat, and the free software movement. You may know more or less about any of these individual influences but, though there will likely be exceptions, anyone looking to engage with the unschool ought to be interested in, critically engaged with, or excited about finding out more about, at least three of the above.

### Central Europe

I am looking at creating a distro and finding likeminded people. Occasional meet-ups in Central Europe (I'm talking Prague or manageably* trainable from Prague) are not a condition so much as an expectation. Planes are avowedly not a welcome substitute. Central Europe used to be a place and a state of mind differently from how it has been so of late, if it has been so at all. For what it's worth** Milan Kundera(from memory)  considered this decline one of the greatest tragedies of the twentieth century.

Prague, for the moment, is the centre. If you can't honestly imagine yourself sitting in a tea house in Prague, Usti nad Labem, Ostrava, or Brno, talking about what you've got yourself involved in, then this particular distro isn't for you.

=> ../index.gmi index

### Notes

  * Night trains and the like are not especially affordable, but collaborators who have the means and / or know-how to arrange safe, comfortable, meaningfully accessible accomodation for meet-ups can help to make it as manageable as it could be meaningful.

  ** His essays are likely worth more than his fiction in my opinion, though some retain many of the faults which make me resistant to read either. The same points are likely made better by others, including perhaps Tony Judt even where he may riff over Kundera.
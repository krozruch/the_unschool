## Visitors Book

You may make a pull request to replace this text:

```
krozruch/the_unschool at codeberg.org

# it will be possible to place some commands here
# though it may equally be better to simply link to a git basics file
# somewhere below
```

Yes, though you have everything here you need to figure out what might be required of you, it might, for many of you, take a lot more figuring out than you are used to. Yes, there's a barrier to entry. Yes, on the other hand, there are those who needn't think at all and would need no more than five minutes of their time. Still, whether five minutes or fifty or five hundred; whether, put differently, you will have invested not 30 seconds of your time per word, to replace these words, or as much as a day and a half or a week or indeed a solid month if perhaps you're into the brevity thing, you can be sure of my valuing that investment of time and effort for what it is, and valuing your words for what they are; and, meanwhile, words of all kinds have elsewhere become so cheap, so devalued.

They used to say that everybody who went to one of those early punk gigs, started their own band. It was an exaggeration, of course, but punk was known for being hands on, an anarchic adhocracy, for it being axiomatic that the protagonists - the "stars" and, better, performers and, there again, instigators and those who lived it - should live lives that are in many respects little different from those who were pogoing to their music or buying their tapes or reading their zines. So it is here. It doesn't take no effort to save up for a bass guitar and an amp, a drum kit and a lock-up to practice in. If it did, it would have been worthless. It doesn't take no effort to learn a handful of chords, or to show up often enough, lugging gear, to mean one can play more or less in tune, more or less in time, to make it all a little something more than the sum of its parts. It doesn't take no effort to get a few friends together who might between them have a little rough-and-ready talent for server administration, a little of the right kind of curiosity to figure out problems relating to protocols and config files or what have you, a little interest in culture, fiction, film, books, or politics or sport or art or quite literally anything else. Anything humans have ever been interested in. What matters is that that effort pays off.

### Afterword (new visitors are to append from the top) 

(If this inspires anyone at all, it should very much remain as little more than an "easter egg", buried below a long list of people - and above the links to the gemsites some of them have set up.)

I remember reading some years back in the New Yorker that Martin Amis believed that the word "frictionless" was emblematic of our times. I suppose he felt that was a good thing. Martin Amis doesn't interest me. Never did. I did a much better job of ignoring his opinions on things than I have of ignoring the opinions of scores of others who didn't interest me but who I have spent far, far too much time thinking about. (I would so willingly give up every fact I have ever gleaned about Elon Musk, every opinion I have ever had about him, though he is far from alone.) I am at this point in time (December, 2023, at the end, I hope, of the hockey stick curve of everything that, after W H Auden, has been low and dishonest about the last decade, following what had, perhaps as late as autumn, been a merely low-key dysfunctional year), so deeply, deeply unimpressed with the result of perhaps literally everything that has ever been described as frictionless. Does it really surprise anybody with knowledge of human affairs that when next to no effort is required of people for them to make a contribution, that their contribution is frequently worthless at best? Or indeed that it can be much, much worse (I'm talking not only Facebook-in-Myanmar levels of worse)?

Google is not flinging these words into your brain at the speed of the proverbial shit off a shovel. Likely as not (if we're close to December, 2023 at least), you had to make a series of non-default choices to get here. And so I am interested in how you got here, and who you are in a way I am not, I promise you, remotely interested in how anybody got to Václavské náměstí (Wenceslas Square).

#### Go and open the door

Years ago, when I was growing up (and I am middle-aged now), I used to play text-based adventure games on a couple of old Acorn computers, when that was a thing. "Go west" one would type, and this, and a bare handful of other such commands and phrases which had to be individually and laboriously built into the typically very much home-rolled and hand-coded logic of the game, would trigger some change, whether it was an unlocked door or simply a change of location. I would guess that, to many of the early enthusiasts of computing, when computing and even programming a computer was thought of more as a hobby than a career path, there was sometimes little experiential difference between these incantations and their result, and the commands (I have taken to calling them "spells") which one would write into the command line, or an irc prompt, or a bulletin board, to "speak to" or leave messages with other humans or indeed to make a computer perform some task. A handful of spells here can permit you to add your name to the above file. A few hundred more, and a handful of friends, could have you set up your own such site.

I'm looking forward to seeing who has opened the door.

=> /unschool/git_basics.gmi Git Basics
